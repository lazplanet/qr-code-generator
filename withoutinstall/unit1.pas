unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtDlgs
  , ubarcodes;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    SavePictureDialog1: TSavePictureDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

  QR: TBarcodeQR;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  try
    QR := TBarcodeQR.Create(nil);
    with QR do begin
      Parent := Form1;
      Top := 0;
      Left := 0;
      SetBounds(320, 10, 300, 300);
      Text := Memo1.Text;
      Show;
    end;
  finally
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if SavePictureDialog1.Execute then
    QR.SaveToFile(SavePictureDialog1.FileName);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  // If QR already created and not freed, free it from memory since we're
  // quiting the program.
  if QR <> nil then QR.Free;
end;

procedure TForm1.Memo1Change(Sender: TObject);
begin
  QR.Text := Memo1.Text;
end;

end.

