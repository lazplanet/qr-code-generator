unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtDlgs,
  ubarcodes;

type

  { TForm1 }

  TForm1 = class(TForm)
    BarcodeQR1: TBarcodeQR;
    Button1: TButton;
    Memo1: TMemo;
    SavePictureDialog1: TSavePictureDialog;
    procedure Button1Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Memo1Change(Sender: TObject);
begin
  BarcodeQR1.Text:=Memo1.Text;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if SavePictureDialog1.Execute then
    BarcodeQR1.SaveToFile(SavePictureDialog1.FileName);
end;

end.

