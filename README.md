# QR Code Generator

Simple project demonstrating how to generate QR Codes with Lazarus IDE using Free Pascal.

Made as a part of [an article for LazPlanet](https://lazplanet.gitlab.io/2021/12/generate-qr-codes.html).

Shows both how to use it with installing component then recompiling Lazarus (`withinstall`) and without installing at all (`withoutinstall`).

![Screenshot](https://gitlab.com/lazplanet/qr-code-generator/-/raw/main/assets/screenshot-01.png "Screenshot")

## Requirements

LazBarcodes library for generating QR Codes. To download:

- Go to <https://sourceforge.net/p/lazarus-ccr/svn/HEAD/tree/components/lazbarcodes/>
- Click **Download Snapshot**

## Run

**withinstall:**

After you downloaded LazBarcodes using above instructions, extract it. Then put it inside the `components` directory.

On Linux/Unix systems you might need to run `sudo unzip lazarus-ccr-svn-*-components-lazbarcodes.zip -d /usr/lib/lazarus/components/` to extract and put it inside components directory.

Refer to the article for details on how to install LazBarcodes. But basically, open `packages/lazbarcodes_runtimeonly.lpk` with Package - Open package file, click Compile. When it shows a green success message (View - Messages if messages are not visible) use the Package - Open Package File again and this time open `packages/lazbarcodes.lpk`, then click Use - Install. Choose to automatically install package, rebuild Lazarus and if shows 2 units with same name, click Ignore All. After Lazarus is restarted, it should be ready to use and there should be a new "Laz Barcodes 2D" tab on toolbar. Since this is now installed, do not move or delete this directory, otherwise Lazarus will have issues.

Just open the .lpi file in Lazarus IDE and click Run - Run or press F9.

**withoutinstall:**

Extract LazBarcodes and rename the extracted directory from something like `lazarus-ccr-svn-r8182-components-lazbarcodes` to `lazbarcodes`. Now put it inside `withoutinstall/libs` so that there is a file named `withoutinstall/libs/lazbarcodes/README.txt`

Just open the .lpi file in Lazarus IDE and click Run - Run or press F9.

## License

See [here](https://gitlab.com/lazplanet/lazplanet.gitlab.io/-/blob/master/LICENSE.md) for license on "project source code".

If you downloaded LazBarcodes, the project has a license: "BSD 3 as it is being inherited from the zint backend source code."
